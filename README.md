# Imagen de Azure DevOps Pipelines Agent

## Variables de ambiente
| Variable  | Descripción |
| --- | --- |
| `AZP_URL` | URL de la instancia de Azure DevOps o Azure DevOps Server |
| `AZP_TOKEN` | Token de Acceso Personal o PAT (Personal Access Token) con alcance (read/manage) de Agent Pools, creado por el usuario que tiene permiso para configurar agentes en `AZP_URL` |
| `AZP_AGENT_NAME` | Nombre del agente (valor por defecto: `hostname del contenedor`) |
| `AZP_POOL` | Nombre del Agent Pool (valor por defecto: `Default`) |
| `AZP_WORK` | Directorio de trabajo (valor por defecto: `_work`) |